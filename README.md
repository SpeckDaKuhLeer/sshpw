# SSHPW Bash Script
Wrapper for sshpass, which is a wrapper for ssh. Has a bunch of cool features
to improve your SSH experience. For a usage guide and full list of features
see below.

You can use it as a drop in replacement for ssh. To give arguments for SSHPW,
write it in front of the host argument. Everything behind it will be given to SSH.

## Usage
```bash
./sshpw [SSHPW OPTION...] [USER@]<HOST/GROUP> [SSH OPTION...]

This script is a wrapper for ssh, which will improve your SSH experience.

ARGUMENT FLAGS:
  -h --help                       print this help page
  -H --print-hostname             print hostname to the console output
  -v --verbose                    turns on debug output
  -V --version                    shows version
     --dry                        dry run to verify your config, WILL SHOW PASSWORDS!
  -r --no-resolve                 disable ip & hostname resolving, take input as it is
  -p --no-password                disable automatic password login from config
  -u --no-user                    disable automatic user testing
  -n --no-connection-test         disable connection test
  -k --no-key                     disable key authentication (-o PubkeyAuthentication=no)
  -C --no-controlmaster           disable controlmaster (-o ControlMaster=no)
  -w --weak                       enable all supported kex algorithms, ciphers and macs
  -g --group                      group mode: enable group mode
  -s --synch                      group mode: enable 'synchronize-panes on' in tmux
  -K --keep                       group mode: keep windows open after closure
  -E --no-encryption-cache        encryption: disable cache function
  -e --clear-encryption-cache     encryption: clear encryption cache
     --encrypt                    encryption: show prompt to encrypt password
     --decrypt                    encryption: show prompt to decrypt password
     --change-intermediate        encryption: change your intermediate password
     --change-master              encryption: change your master encryption password

ARGUMENT SETTINGS:
  -c --config <file>              alternative ssh configuration
  -t --test-timeout <seconds>     specifies the timeout while testing the connection (default 1s)
  -T --test-retries <count>       specifies the number of tests for each IP (default 3)
  -l --login-attempts <count>     specifies the number of login attempts (default 10)
  -P --password-variable <string> variable part for password generation, can be used with %%V%%
     --overwrite-host <string>    overwrite host/ connect part in password generation
     --overwrite-fqdn <string>    overwrite FQDN part in password generation
     --overwrite-ip <string>      overwrite IP part in password generation
     --overwrite-port <string>    overwrite port part in password generation
     --overwrite-user <string>    overwrite user part in password generation

SSH CONFIG GLOBAL:
  ##!Domain <searchdomain>          extra searchdomains
  ##!Group <group> <host>           will connect to all of mentioned hosts if group mode is active
  ##!Blacklist <regex>              if ip will match on this, script will stop (pre whitelist)
  ##!Whitelist <regex>              if list not empty only matching IPs will work

SSH CONFIG HOST:
  ##!Password <password>            password that will be used
  ##!PasswordFile <file>            password file that will be used
  ##!PasswordUser <user> <password> password that will be used
  ##!PasswordUserFile <user> <file> password file that will be used

FILE VARIABLES:
  %%H%%                         host argument <HOST>
  %%F%%                         full FQDN
  %%U%%                         username
  %%I%%                         resolved IP
  %%P%%                         SSH port
  %%V%%                         variable part, can be given using -P --password-variable
  $()                           shellcode, variables will also be replaced, \" must be escaped (\\")
```

## Installation
Copy or symlink the sshpw script into a directory which is included in your
PATH variable. Works out of the box, without configuration. Be productive.

## Features/ Usage
The script will do the following things in order:
* parse your SSHPW arguments
* check if all required programs are installed
* locate the SSH configuration you are using currently (first in your argument, than your home, than /etc)
* check if you redirected a pipe into the script, it will wait until you got an end of file
* verify all global configuration entries (for example ##!Domain)
* if group mode is active it will start a tmux session and reexecute it self for every host in the group
* verify all special arguments that got added using SSHPW (for example -k)
* get all ssh configuration for the argument you entered
* try to identify the SSH port (first argument, than configuration, than 22)
* it will try to get a hostname from the configuration using the given argument (if found it will replace the argument with the one from the configuration)
* it will try to get a working IP address using ncat by test the SSH port
* it will check if the IP address is black our white listed
* it will parse the configuration again using the FQDN
* it will parse the configuration again using the IP
* it will try to get all users and passwords from the configuration in order
* it will try to login using your SSH key
* it will try to login using the passwords and users (it will prefer specific passwords for a user before default ones)
* if nothing else works it will try to use plain SSH

The main function of the script is to automatically login into devices which
do not support password less login. You can specify a password or passwords
in the configuration that will be tested from the top to the bottom.
If you users that require a special password you can use the password user
option. Find more information about this in the configuration examples.
Meanwhile this script does a lot more.

You will be able to configure special sshpw settings in your SSH configuration.
All of them start with ##!, which will not affect your normal SSH configuration,
as they start with a comment.
You have to differentiate between host specific configuration, which will only
match underneath a host entry and global configuration, which can be anywhere
in the configuration.
All of the configurations are case insensitive.

### Automatic login
You can specify different passwords in the configuration that will be used
to login. Passwords that are on the top will be tested first. All password
related configuration is host specific, therefor you have to write them
underneath a host entry.

Use ##!Password or ##!PasswordFile to specify a password within the
configuration, or link to a password file, which can have more than
one password (one on each line in the file).

The ##!PasswordUser and ##!PasswordUserFile will do the same, but it
will only be used when the specified user will be used to login.

### Password generation
You can automatically generate passwords using keywords within it.
See --help for all of them.

### Password encryption
SSHPW is able to encrypt passwords using openssl. When you first use this
feature, you will be asked for a master and intermediate key.
The encryption will use 3 different passwords, but is actually very simple
to use.

SSHPW is able to detect encrypted passwords using the prefix SSHPWENC.
You can use encrypted passwords everywhere instead of clear text passwords,
mix them up as you wish.

Your actual encryption key. This is a very long randomly generated password,
which you will not see. It will be stored in your local ssh folder.
This key will be used to encrypt and decrypt all the password that you will
use in your configuration.

Your master key. This is a (at best) long key, which will be used to encrypt
you encryption key and your intermediate key.

You intermediate key. This can be empty, which is not recommended. It will
be used to encrypt your encryption key in RAM.

Once you encrypted passwords you will be prompted for your master key, once
per reboot. After that the encryption key will be cached in RAM. Everytime
you start SSHPW and use encrypted passwords, you will be prompted for the
intermediate key.

### Automatic domain resolution
SSHPW will try to resolve any given host argument using your system DNS
settings. If this does not work it will try to use the domains provided
in your configuration. Linux does only support a certain number of
search domains. Because of that you able to configure them in the SSH
configuration.

Use the domain setting to specify additional domains. They will be checked
from the top to the bottom, therefor set the most used at the top. You can
write them anywhere in your configuration, as they are a global setting.

```bash
##!Domain subdomain.com
##!Domain com
```

### Group mode
Group mode will open a tmux session with all the hosts within the
mentioned group. 

It will unset the TMUX variable, which means you can have nested tmux sessions.
Keep in mind that you have to press your tmux escape sequence twice to pass
commands to the inner tmux session.

### Whitelist and Blacklist
With this feature you can whitelist or blacklist IP addresses using regex
expressions. The blacklist will match first. If the whitelist is empty,
everything will be allowed by default.

See in the configuration example for more information.

### Verbose/ -v --verbose
Use the verbose option to debug the behavior of the script. If you think
you encountered a bug, or do not understand what your configuration does,
this will give you most of the information that you should need.

### Dry run/ --dry
If something weird happens while logging in into the target or you are not
sure if your password generation works use this. It will show the whole SSH
command that will be executed in the end.

This will output passwords!

### Print hostname/ -H --print-hostname
This will print the hostname you are connecting to infront of the connection.
Very usefull in combination with group mode so that you know which hosts you
are connected to.

### No resolve/ -r --no-resolve
This will disable automatic resolving of the given argument. The script will
always try to login using the FQDN if possible. It will still make DNS lookups
to get the correct parameters for the password generation, but it will use
the given argument to connect.

### No password login/ -p --no-password
The script will not try to login using the different passwords from the
configuration. This will disable the usage of sshpass.

### No user testing/ -u --no-user
This script will not try to login using the different users from the
configuration. User matching will only be done by SSH it self.

### No connection test/ -n --no-connection-test
This will disable the connection test to the target system. This can increase
performance but might lead to connection refused errors if the target should
have more than one IP address.

### No key authentication/ -k --no-key
This will disable key authentication. The script will not test the connection
using the SSH key.

### No controlmaster/ -C --no-controlmaster
Simply pass the option to disable the control master to SSH.

### Enable weak cryptographic algorithms/ -w --weak
This will enable all supported algorithms by the system. Not recommended.

### Group mode/ -g --group
Enable group mode. Enter the group mode instead of the host you
would connect to.

```bash
sshpw -g servers
```

### Group mode synch/ -s --synch
This will send "set synchronize-panes on" to the tmux session. Every keypress
will be sent to every host within the open session.

### Group mode keep/ -K --keep
This will tell tmux to keep the windows open, even if the SSH session has been
terminated. This can be for example used, when you can not connect to a host.
You will able to read the error message in this case.

### Encryption disable cache/ -E --no-encryption-cache
This will disable the caching function, when using encrypted passwords.

### Encryption clear cache/ -e --clear-encryption-cache
This will remove the cached key. You will be again prompted for your master key.

### Encryption encrypt/ --encrypt
This will encrypt a password.

### Encryption decrypt/ --decrypt
This will decrypt a password.

### Encryption change intermediate key/ --change-intermediate
This will change your intermediate key.

### Encryption change master key/ --change-master
This will change your master key.

### Alternative ssh config location/ -c --config
This will specify and alternative ssh configuration location.

### SSH port test timeout/ -t --test-timeout
This will specify the number of seconds SSHPW will wait for the ssh port
connection test.

### SSH port test retries/ -T --test-retries
This will specify the number of times SSHPW will test the connection to
every IP address that could be resolved.

If 3 IPs will be resolved it will test them 3 times each per default.
1-2-3-1-2-3-1-2-3

### Maximum number of login attempts/ -l --login-attempts
This will specify the maximum number of login attempts SSHPW will use
while connecting. If most of your passwords do not work and the system
only supports 3 authentication attempts from an IP address in series,
decrease this setting, or you will log your self out.
